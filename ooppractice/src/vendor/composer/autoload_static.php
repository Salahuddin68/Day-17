<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit535663e8f448536a92b8f2c5e3d522e7
{
    public static $prefixLengthsPsr4 = array (
        'B' => 
        array (
            'Bitm\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Bitm\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit535663e8f448536a92b8f2c5e3d522e7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit535663e8f448536a92b8f2c5e3d522e7::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
